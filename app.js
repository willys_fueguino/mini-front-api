const express = require("express");
const app = express();
require("dotenv").config();
const port = process.env.PORT;
const morgan = require("morgan")

app.use(express.json())
app.use(morgan("dev"))
app.use(function(req, res, next) {  
    res.header('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});  

app.post("/hola", async (req,res)  => {
    let mensaje = await JSON.stringify(req.body.mensaje)
    console.log("req: " + mensaje)
    try{
        
        if(mensaje == '"hola"'){
            res.status(200).json("Chauu!!!")
        } else {
            res.status(403).json("Codigo incorrecto")
        }
    
    }
    catch(err){
        console.log(err)
    }
})

app.listen(port, () => console.log("Servidor iniciado en el puerto: " + port))